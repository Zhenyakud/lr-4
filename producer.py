import time
from datetime import datetime
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=["localhosy:9092"])

if __name__ == "__main__":
    for i in range(1,1000):
        producer.send("lab4"), str.encode(str(datetime.now()))

    print('Messages have been sent')
