from kafka import KafkaProducer

consumer = KafkaConsumer('lab4', group_id='1', bootstrap_servers=['localhost:9092'])

for message in consumer:
    print(message.value.decode("utf-8"))
    